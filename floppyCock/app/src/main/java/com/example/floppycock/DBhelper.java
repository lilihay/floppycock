package com.example.floppycock;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.sql.SQLException;

public class DBhelper extends SQLiteOpenHelper {

    public DBhelper(Context context) {
        super(context, "UsersInfo.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatement = "CREATE TABLE IF NOT EXISTS USER(HIGH_SCORE INTEGER)";
        db.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS USER");
    }

    public void updateHighScore(int highScore) {
        SQLiteDatabase db = this.getWritableDatabase();

        File file = new File("UsersInfo.db");

        if (file.exists()) //here's how to check
        {
            ContentValues newValues = new ContentValues();
            newValues.put("HIGH_SCORE", highScore);

            db.update("USERS", newValues, "id=1", null);
        }
        else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("HIGH_SCORE", highScore);

            db.insert("USER", null, contentValues);

            db.close();
        }
    }

    public int getHighScore(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT HIGH_SCORE FROM USER WHERE rowid = 1;", new String[]{});

        if(cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        cursor.close();
        return 0;
    }
}

