package com.example.floppycock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    int texture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        texture = intent.getIntExtra("texture", R.drawable.cock);
        ImageView img= (ImageView) findViewById(R.id.imageView);
        img.setImageResource(texture);
    }

    public void startGame(View view) {
        Intent intent = new Intent(this, Game.class);
        intent.putExtra("texture", texture);
        startActivity(intent);
        this.finish();
    }

    public void QuitGame(View view){
    Dialog dialog = new Dialog();
    dialog.show(getSupportFragmentManager(), "Exit");
    }

    public void SkinMenu (View view) {
        Intent intent = new Intent(this, SkinSelect.class);
        startActivity(intent);
        this.finish();
    }
}